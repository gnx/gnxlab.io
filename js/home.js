---
---
function initMap(container) {
	const map = new L.Map(container);
  
  const defaultLayer = L.tileLayer.provider('OpenStreetMap.Mapnik').addTo(map);

  const baseLayers = {
    'OpenStreetMap Default': defaultLayer,
    'OpenTopoMap': L.tileLayer.provider('OpenTopoMap'),
    //'OpenStreetMap German Style': L.tileLayer.provider('OpenStreetMap.DE'),
    //'OpenStreetMap H.O.T.': L.tileLayer.provider('OpenStreetMap.HOT'),
    //'Thunderforest OpenCycleMap': L.tileLayer.provider('Thunderforest.OpenCycleMap'),
    //'Thunderforest Transport': L.tileLayer.provider('Thunderforest.Transport'),
    //'Thunderforest Landscape': L.tileLayer.provider('Thunderforest.Landscape'),
    //'Hydda Full': L.tileLayer.provider('Hydda.Full'),
    //'Stamen Toner': L.tileLayer.provider('Stamen.Toner'),
    //'Stamen Terrain': L.tileLayer.provider('Stamen.Terrain'),
    //'Stamen Watercolor': L.tileLayer.provider('Stamen.Watercolor'),
    //'Jawg Streets': L.tileLayer.provider('Jawg.Streets'),
    //'Jawg Terrain': L.tileLayer.provider('Jawg.Terrain'),
    //'Esri WorldStreetMap': L.tileLayer.provider('Esri.WorldStreetMap'),
    //'Esri DeLorme': L.tileLayer.provider('Esri.DeLorme'),
    //'Esri WorldTopoMap': L.tileLayer.provider('Esri.WorldTopoMap'),
    'Esri WorldImagery': L.tileLayer.provider('Esri.WorldImagery'),
    //'Esri WorldTerrain': L.tileLayer.provider('Esri.WorldTerrain'),
    //'Esri WorldShadedRelief': L.tileLayer.provider('Esri.WorldShadedRelief'),
    //'Esri WorldPhysical': L.tileLayer.provider('Esri.WorldPhysical'),
    //'Esri OceanBasemap': L.tileLayer.provider('Esri.OceanBasemap'),
    //'Esri NatGeoWorldMap': L.tileLayer.provider('Esri.NatGeoWorldMap'),
    //'Esri WorldGrayCanvas': L.tileLayer.provider('Esri.WorldGrayCanvas'),
    //'Geoportail France Maps': L.tileLayer.provider('GeoportailFrance'),
    //'Geoportail France Orthos': L.tileLayer.provider('GeoportailFrance.orthos'),
    //'USGS USTopo': L.tileLayer.provider('USGS.USTopo'),
    //'USGS USImagery': L.tileLayer.provider('USGS.USImagery'),
    //'USGS USImageryTopo': L.tileLayer.provider('USGS.USImageryTopo'),
  };

  const overlayLayers = {
    //'OpenSeaMap': L.tileLayer.provider('OpenSeaMap'),
    //'OpenWeatherMap Clouds': L.tileLayer.provider('OpenWeatherMap.Clouds'),
    //'OpenWeatherMap CloudsClassic': L.tileLayer.provider('OpenWeatherMap.CloudsClassic'),
    //'OpenWeatherMap Precipitation': L.tileLayer.provider('OpenWeatherMap.Precipitation'),
    //'OpenWeatherMap PrecipitationClassic': L.tileLayer.provider('OpenWeatherMap.PrecipitationClassic'),
    //'OpenWeatherMap Rain': L.tileLayer.provider('OpenWeatherMap.Rain'),
    //'OpenWeatherMap RainClassic': L.tileLayer.provider('OpenWeatherMap.RainClassic'),
    //'OpenWeatherMap Pressure': L.tileLayer.provider('OpenWeatherMap.Pressure'),
    //'OpenWeatherMap PressureContour': L.tileLayer.provider('OpenWeatherMap.PressureContour'),
    //'OpenWeatherMap Wind': L.tileLayer.provider('OpenWeatherMap.Wind'),
    //'OpenWeatherMap Temperature': L.tileLayer.provider('OpenWeatherMap.Temperature'),
    //'OpenWeatherMap Snow': L.tileLayer.provider('OpenWeatherMap.Snow'),
    //'Geoportail France Parcels': L.tileLayer.provider('GeoportailFrance.parcels'),
    //'Waymarked Trails Hiking': L.tileLayer.provider('WaymarkedTrails.hiking'),
    //'Waymarked Trails Cycling': L.tileLayer.provider('WaymarkedTrails.cycling'),
    //'Waymarked Trails MTB': L.tileLayer.provider('WaymarkedTrails.mtb'),
    //'Waymarked Trails Ski Slopes': L.tileLayer.provider('WaymarkedTrails.slopes'),
    //'Waymarked Trails Riding': L.tileLayer.provider('WaymarkedTrails.riding'),
    //'Waymarked Trails Skating': L.tileLayer.provider('WaymarkedTrails.skating')
  };

  L.control.layers(baseLayers, overlayLayers, {collapsed: false}).addTo(map);

	map.setView(new L.LatLng(42.8805, 25.3180),11); // zoom to Gabrovo

	return map;
}

const homeMap = initMap("home-map");
let homeData = {{ site.data.home | jsonify }};
// sort them, so the one I am most interested in are not hidden by the less interesting ones
// homeData = homeData.sort((a, b) => a.интерес - b.интерес); // commented out, because that sort does not reorder the markers

for (const home of homeData) {
  console.log(home);
  function getIcon(interest) {
    switch(interest) {
      case -1: return blackIconSmall;
      case 0: return blueIcon;
      case 1: return yellowIcon;
      case 2: return greenIcon;
      case 3: return greenIcon2x;
    }
    return greyIcon;
  }
  function getColor(interest) {
    switch(interest) {
      case -1: return "black";
      case 0: return "blue";
      case 1: return "yellow";
      case 2: return "green";
      case 3: return "green";
    }
    return "grey";
  }
  
  let markerCoords;
  if (home.интерес < 1) continue; // TODO: Temporary cleaner. Remove after we buy the property
  if (home.точки) {
    const nodes = home.точки.map((node) => {
      const splt = node.split(",");
      const x = Number(splt[0]);
      const y = Number(splt[1]);
      return [x, y];
    });
    const polygon = L.polygon(nodes, {color: getColor(home.интерес)});
    polygon.addTo(homeMap);
    
    // taken (and adapted) from https://stackoverflow.com/questions/9692448/how-can-you-find-the-centroid-of-a-concave-irregular-polygon-in-javascript
    function getPolygonCentroid(pts) {
      const first = pts[0], last = pts[pts.length-1];
      if (first[0] != last[0] || first[1] != last[1]) pts.push(first);
      let twicearea = 0,
      x=0, y=0,
      nPts = pts.length,
      p1, p2, f;
      for (let i = 0, j = nPts - 1 ; i < nPts ; j = i++) {
        p1 = pts[i]; p2 = pts[j];
        f = p1[0]*p2[1] - p2[0]*p1[1];
        twicearea += f;          
        x += ( p1[0] + p2[0] ) * f;
        y += ( p1[1] + p2[1] ) * f;
      }
      f = twicearea * 3;
      return [x/f, y/f];
    }
    markerCoords = getPolygonCentroid(nodes);
  } else {
    markerCoords = [home.ширина, home.дължина];
  }
  const marker = L.marker(markerCoords, {icon: getIcon(home.интерес)});
  marker.bindPopup(buildPopupMessage(home));
  if (home.цена && home.интерес > 0) {
    const formatter = new Intl.NumberFormat('bg-BG', {
      style: 'currency',
      currency: 'BGN',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0  
    });
    const text = formatter.format(home.цена);
    marker.bindTooltip(text, {
      permanent: true, 
      direction: 'right'
    }).openTooltip();
  }
  
  marker.addTo(homeMap);
  
  
  if (home.неопределеност) {
    const circle = L.circle([home.ширина, home.дължина], {
      color: getColor(home.интерес),
      fillColor: getColor(home.интерес),
      fillOpacity: 0.5,
      radius: home.неопределеност
    });
    circle.addTo(homeMap);
  }
}

function buildPopupMessage(home) {
  let content = "";
  if (home.напредък) content += `<div>${home.напредък.toUpperCase()}</div>`;
  if (home.вид) content += `<div>${home.вид}</div>`;
  content += '<table class="table"><thead><tr><th>цена</th><th>площ</th></tr></thead><tbody><tr>';
  if (home.цена) {
    const formatter = new Intl.NumberFormat('bg-BG', {
      style: 'currency',
      currency: 'BGN',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0  
    });
    content += `<td>${formatter.format(home.цена)}</td>`;
  }
  if (home.площ) {
    const formatter = new Intl.NumberFormat('bg-BG');
    content += `<td>${formatter.format(home.площ)} m<sup>2</sup></td>`;
  }
  content += "</tr></tbody></table>";
  if (home.снимки) {
    content += "<div>";
	  for (const snimka of home.снимки) {
      content += `<img class="spring-picture" src="${snimka}">`;
	  }
    content += "</div>";
  }
  if (home.обяви) {
    content += '<table class="table"><thead><tr><th>агенция</th><th>брокер</th><th>препратки</th></tr></thead><tbody>';
	  for (const obqva of home.обяви) {
      content += "<tr>";
      content += `<td>${obqva.агенция || ""}</td>`;
      content += `<td>${obqva.брокер || ""}</td>`;
      content += "<td>";
      if (obqva.препратки) {
        for (const prepratka of obqva.препратки) {
          const domain = (new URL(prepratka)).hostname.replace(/www.|.bg|.com/, "");
          content += `<div><a href="${prepratka}">${domain}</a></div>`;
        }
      }
      content += "</td>";
      content += "</tr>";
	  }
    content += "</tbody></table>";
  }
  
  if (home.кадастър) {
    content += `<div>№ в кадастъра: <strong>${home.кадастър}</strong></div>`;
  }
  /* TODO: Temporary cleaner. Remove after we buy the broperty
  if (home.плюсове && home.минуси) {
    content += '<table class="table"><thead><tr><th>+</th><th>-</th></tr></thead><tbody><tr>';
    const largest = Math.max(home.плюсове.length, home.минуси.length);
    for (let i = 0; i < largest; i++) {
      content += "<tr>";
      content += `<td class="pluses">${home.плюсове[i] || ""}</td>`;
      content += `<td class="minuses">${home.минуси[i] || ""}</td>`;
      content += "</tr>";
    }     
    content += "</tr></tbody></table>";
  } else if (home.плюсове) {
    content += "<ul>";
    for (const plus of home.плюсове) {
      content += `<li class="pluses">${plus}</li>`;
    }
    content += "</ul>";
  } else if (home.минуси) {
    content += "<ul>";
    for (const minus of home.минуси) {
      content += `<li class="minuses">${minus}</li>`;
    }
    content += "</ul>";
  }
  */
  
  return content;
}

// click display coordinates
homeMap.on("contextmenu", (event) => {
  function roundCoord(value) {
    const precision = 5;
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }
  console.log(`Кликна на:\n${roundCoord(event.latlng.lat)},${roundCoord(event.latlng.lng)}`);
});