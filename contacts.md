---
title: Връзка с мен
---
е-поща: [hristo.dr.hristov@gmail.com](mailto:hristo.dr.hristov@gmail.com)  
Element: [@hristo.hristov:matrix.org](https://app.element.io/#/user/@hristo.hristov:matrix.org)   
Diaspora: [archri@diasp.org](https://diasp.org/people/2a10cde09f66013398b7782bcb452bd5)  
GitLab: [gnx](https://gitlab.com/gnx)  
GitHub: [hristo-d-hristov](https://github.com/hristo-d-hristov)
