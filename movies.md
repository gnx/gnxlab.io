---
title: Любими филми
---
{% assign movies_sorted_by_year_and_name = site.data.movies | sort:"name" | sort:"year" | reverse %}
{% assign sorted_movies = movies_sorted_by_year_and_name | sort:"rating" | reverse %}
{% for movie in sorted_movies %}
  {% assign rating = movie.rating | times: 1 %}
  {% if rating > 0 %}
[{{ movie.name }} ({{ movie.year }})](http://www.imdb.com/title/tt{{ movie.imdb_id }})
  {% endif %}
{% endfor %}
