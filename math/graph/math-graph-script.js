const themes = [[
      {"group": 1, "icon": "10", "id": "естествени до 10"},
      {"group": 1, "icon": "20", "id": "естествени до 20"},
      {"group": 2, "icon": "100", "id": "естествени до 100"},
      {"group": 3, "icon": "1000", "id": "естествени до 1000"},
      {"group": 4, "icon": "100 000 000 ...", "id": "всички естествени"},
      {"group": 5, "icon": "1/2", "id": "дроби"},
      {"group": 6, "icon": "-1", "id": "отрицателни"},
      {"group": 7, "icon": "a", "id": "константи и променливи"},
      {"group": 8, "icon": "√", "id": "ирационални"},
      {"group": 12, "icon": "k", "id": "комплексни"},
    ],[
      {"group": 9, "icon": "sin90", "id": "триг. до 90°"},
      {"group": 10, "icon": "sin180", "id": "триг. до 90°"},
      {"group": 11, "icon": "sin360k", "id": "триг. на обобщен ъгъл"}
    ],[
      {"group": 1, "icon": "+", "id": "събиране"},
      {"group": 1, "icon": "-", "id": "изваждане"},
      {"group": 2, "icon": "*", "id": "умножение"},
      {"group": 2, "icon": "/", "id": "деление"},
      {"group": 5, "icon": "^", "id": "степенуване"},
      {"group": 6, "icon": "^", "id": "абсолютна стойност"},
      {"group": 8, "icon": "√", "id": "коренуване"},
      {"group": 11, "icon": "n√", "id": "корен n-ти"},
      {"group": 11, "icon": "log", "id": "логаритмуване"}
    ],[
      {"group": 1, "icon": "□", "id": "квадратна мрежа"},
      {"group": 3, "icon": "▫", "id": "милиметрова хартия"},
      {"group": 5, "icon": "📊", "id": "графики и диаграми"},
      {"group": 6, "icon": "⦝", "id": "декартова координатна система"},
      {"group": 9, "icon": "↷", "id": "графика на функция"},
      {"group": 10, "icon": "∿", "id": "графика на тригонометрична функция"},
      {"group": 12, "icon": "↝", "id": "анализ"}
    ],[
      {"group": 8, "icon": "⧢", "id": "комбинаторика"},
      {"group": 9, "icon": "⧢", "id": "вероятности"},
      {"group": 10, "icon": "🗠", "id": "статистика"}
    ],[
      {"group": 2, "icon": "x+-", "id": "неизвестно събираемо, умаляемо и умалител"},
      {"group": 3, "icon": "x*/", "id": "неизвесни множител, делимо и делител"},
      {"group": 6, "icon": "x^", "id": "неизвесни основа или степенен показател"},
      {"group": 6, "icon": "x=", "id": "уравнения"},
      {"group": 7, "icon": "x<>", "id": "неравенства"},
      {"group": 7, "icon": "x^2", "id": "квадратни уравнения"},
      {"group": 9, "icon": "xy", "id": "системи"},
      {"group": 11, "icon": "xy", "id": "тригонометрични уравнения"}
    ],[
      {"group": 1, "icon": "–", "id": "отсечка"},
      {"group": 3, "icon": "∢", "id": "ъгъл"},
      {"group": 1, "icon": "△", "id": "триъгълник"},
      {"group": 1, "icon": "□", "id": "квадрат"},
      {"group": 1, "icon": "▭", "id": "правоъгълник"},
      {"group": 5, "icon": "⬡", "id": "многоъгълник"},
      {"group": 5, "icon": "○", "id": "кръг"}
    ],[
      {"group": 5, "icon": "🤖", "id": "ръбести тела"},    
      {"group": 6, "icon": "🎩", "id": "валчести тела"},
      {"group": 10, "icon": "🍽", "id": "стереометрия"},
      {"group": 12, "icon": "🍞", "id": "принцип на Кавалиери"}
    ]
  ];

var links = [
  //{"source": "триъгълник", "target": "тригонометрични функции до 90°", "value": 5, visible: true},
];
var color = 1;
for (themeGroup of themes) {
  var prevThemeId = themeGroup[0].id;
  themeGroup[0].colorNum = color;
  for (j = 1; j < themeGroup.length; j++) {
    var thisThemeId = themeGroup[j].id;
    var link = {"source": prevThemeId, "target": thisThemeId, "value": 5, "type": "threadLink", "isVisible": true};
    links.push(link);
    prevThemeId = thisThemeId;
    themeGroup[j].colorNum = color; // ако ползвам просто color не работи ... явно е запазено поле
  }
  color++;
}

const flattened = [].concat(...themes);

var classes = [];
for (var i = 1; i < 13; i++) {
  var node = {"group": i, "id": i, "val": 50, "isClassNode": true};
  classes.push(node);
}

var nodes = flattened.concat(classes);

for (var i = 1; i < 12; i++) {
  var link = {"source": i, "target": i+1, "type": "classesLink", "isVisible": true};
  links.push(link);
}

for (theme of flattened) {
  var link = {"source": theme.id, "target": theme.group, "type": "nodeToClassLink"};
  links.push(link);
}

const gData = {
  "nodes": nodes,
  "links": links
};

const Graph = ForceGraph3D({ controlType: 'orbit' })
(document.getElementById('3d-graph'))
  //.cooldownTicks(50) // това е нужно за zoomToFit
  .graphData(gData)
  .nodeLabel('id')
  .nodeAutoColorBy('colorNum')
  .linkDirectionalParticles("value")
  .linkDirectionalParticleSpeed(d => d.value * 0.001)
  .linkDirectionalParticleWidth(2)
  .linkVisibility(d => d.isVisible)
  .nodeThreeObject(node => {
    if (!node.isClassNode) {
    const sprite = new SpriteText(node.id);
    sprite.material.depthWrite = false; // make sprite background transparent
    sprite.color = node.color;
    sprite.textHeight = 8;
    return sprite;
  }
  });

const linkForce = Graph
      .d3Force('link')
      .distance(link => link.type === "nodeToClassLink" ? 100 : 10);
// fit to canvas when engine stops
//Graph.onEngineStop(() => Graph.zoomToFit(400));