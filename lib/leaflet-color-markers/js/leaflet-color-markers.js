var blueIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-blue.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var redIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-red.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greenIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-green.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var orangeIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-orange.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var yellowIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-yellow.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var violetIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-violet.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greyIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-grey.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var blackIcon = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-black.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var blackIconSmall = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-black.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [12, 20],
	iconAnchor: [6, 20],
	popupAnchor: [1, -17],
	shadowSize: [20, 20]
});

var blueIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-blue.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var redIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-red.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greenIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-green.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [37, 61],
	iconAnchor: [18, 61],
	popupAnchor: [1, -51],
	shadowSize: [41, 41]
});

var orangeIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-orange.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var yellowIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-yellow.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var violetIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-violet.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greyIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-grey.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var blackIcon2x = new L.Icon({
	iconUrl: './lib/leaflet-color-markers/img/marker-icon-2x-black.png',
	shadowUrl: './lib/leaflet-color-markers/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});
